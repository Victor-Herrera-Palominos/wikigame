Static page which shows off a version with only HTML and JavaScript/AngularJS: https://victor-herrera-palominos.bitbucket.io/

Built on CodeIgniter using the MVC model, with the AngularJS framework and SQL to communicate with the database

After creating a database, application/config/database.php must be updated with values for 
the username and password of a user with necessary privileges, and your chosen name for the database.

The following SQL query will create the table needed: 

CREATE TABLE `results` (

 `id` int(10) NOT NULL AUTO_INCREMENT,
 
 `User` varchar(30) NOT NULL,
 
 `Path` varchar(1000) NOT NULL,
 
 PRIMARY KEY (`id`)
)

